package com.example.mame;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MemeAdapter extends RecyclerView.Adapter<MemeAdapter.ViewHolder> {
    private final List<Meme> memes;
    private OnItemClickListener mListener;
    public interface OnItemClickListener{
        void onItemClick(int position);

        void downloadImage(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }
    public MemeAdapter(Context applicationContext, List<Meme> memes) {
        this.memes = memes;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.meme_list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MemeAdapter.ViewHolder holder, int position) {
        //String memetitle = memes.get(position).getTitle();
        String memeimage = memes.get(position).getMeme();
        String shareurl = memeimage;
        holder.setData(memeimage);
        //Picasso.get().load(memes.get(position).getMeme()).into(holder.currentmeme);
    }

    @Override
    public int getItemCount() {
        return memes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        //private TextView currenttitle;
        ImageView currentmeme;
        ImageView shareIcon;
        ImageView download_image;
        public ViewHolder(View itemView) {
            super(itemView);
            //currenttitle = itemView.findViewById(R.id.memetitle);
            currentmeme = itemView.findViewById(R.id.meme);
            shareIcon = itemView.findViewById(R.id.share);
            download_image = itemView.findViewById(R.id.download);
            //currentmeme = (ImageView) itemView.findViewById(R.id.meme);

            shareIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            mListener.onItemClick(position);
                        }
                    }
                }
            });
            download_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            mListener.downloadImage(position);
                        }
                    }
                }
            });


        }

        public void setData(String memeImage) {
            //currenttitle.setText(memetitle);
            //currentmeme.setImageBitmap(memeImage);
            Picasso.get().load(memeImage).into(currentmeme);

        }
    }






//    public MemeAdapter(Activity context, ArrayList<Meme> memes) {
//        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
//        // the second argument is used when the ArrayAdapter is populating a single TextView.
//        super(context, 0, memes);
//    }
//
//
//    @NonNull
//    @Override
//    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        // Check if the existing view is being reused, otherwise inflate the view
//        View listItemView = convertView;
//        if(listItemView == null) {
//            listItemView = LayoutInflater.from(getContext()).inflate(
//                    R.layout.meme_list_item, parent, false);
//        }
//        Meme currentmeme = getItem(position);
//        String memeTitle = currentmeme.getTitle();
//        TextView textView = (TextView) listItemView.findViewById(R.id.title);
//        textView.setText(memeTitle);
//        Bitmap memeImage = currentmeme.getMeme();
//        ImageView imageView = (ImageView) listItemView.findViewById(R.id.meme);
//        imageView.setImageBitmap(memeImage);
//        return listItemView;
//    }
}
